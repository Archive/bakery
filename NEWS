2.6.3:

* document_history_add(): Check that the file really exists, because 
  GtkRecentManager issues a critical warning otherwise.
  (Murray Cumming, Johannes Schmid)
* Build: Fixed a compiler warning in the examples.
  (Johannes Schmid)

2.6.2:

* Mark the File, Edit, and Help menu titles for translation.
  Ubuntu bug https://bugs.launchpad.net/ubuntu/+source/glom/+bug/306942
  (elmergato)

2.6.1:

* Correctly register the document with the recent files system even if 
  the file (such as a template) was resaved while opening.
  (Murray Cumming)

2.6.0:

(Bakery-2.6 installs in parallel with bakery-2.4.)

* Document_XML::get_node_child_named(): Ignore TextNodes.
* Document_XML::write_to_disk(): Avoid a refcount problem with 
  glibmm 2.16.0,  though that is not necessary now. The fix is 
  harmless with fixed glibmm versions.
  (Murray Cumming)
* Maemo build fix.
  (Armin Burgmeier)

2.5.1:

This is a new bakery-2.6 API/ABI. It installs in parallel with bakery-2.4.

* Ported to giomm (part of glibmm), 
  removing the dependency on gnome-vfsmm or gnome-vfs.
* Document_XML: Added add_indenting_white_space_to_node() for more useful indenting 
  than libxml gives (because libxml gives up when it finds a text node, 
  somewhat understandably).
  Let's see how well this works. It is probably very bad for some documents.

2.4.4:

* Document: Added load_from_data().
* App_WithDoc: Added open_document_from_data().
  These can be used when the document's data is not received via 
  a URI. For instance, over the network via a special protocol.
  (Murray Cumming)
* Build: Don't require gnome-vfs on Windows (for the MIME type), 
  to simplify the build, and make some other fixes for the Windows 
  build.
  (Armin Burgmeier)

2.4.3:

* Fix the build when deprecated API is disabled in gtkmm.
  (Armin Burgmeier, Murray Cumming)
* Now depends on gtkmm 2.10.
  (Murray Cumming)

2.4.2:

* Experimental Maemo support,
  using hildon_* API instead of GTK+ or GNOME API, 
  for instance for dialogs.
  (Armin Burgmeier)
* Fix the build when glibmm was built with exceptions 
  disabled.
  (Armin Burgmeier)

2.4.1:

* New translations:
  - Arabic (Djihed Afifi)
  - Bulgarian (Petar Tahchiev, Alexander Shopov)
  - Czech (Václav Čermák, Jakub Friedl)
  - Japanese (Satoru SATOH)
  - Macedonian (Jovan Naumovski)
  - Norwegian Bookmal (Kjartan Maraas)
  - Russian (Valek Filippov, Nickolay V. Shmyrev)

* Updated translations:
  - Nepali (Pawan Chitrakar)

2.4.0

- Released API/ABI-stable version.
- Some const fixes.

2.3.18:

* BusyCursor: Added a constructor that takes a window pointer, 
  as well as the existing constructor that takes a reference, 
  so that application code does not need to test for a null 
  parent window and thus lose the advantage of scope lifetime.

2.3.17:

* Save As:
  - Still offer the existing directory, but not the existing 
    file, because it's not a good default.
  - Warn and ask again if the chose file is read-only, or the 
    directory is read-only, though I think GtkFileChooser 
    should do this.
* New translations:
  - Vietnamese (Clytie Siddall)
  - Finnish (Ilkka Tuohela)
* Updated translations:
  - Brazilian Portuguese (Raphael Higino)
   

2.3.16:

* Saving:
  - Use the Gtk::FileChooser confirm-overwrite feature 
  instead of our own dialog.
  - Avoid warning when using Save As.
* Translations: Added Catalan (Josep Puigdemont), 
  Dutch (Vincent van Adrighem), Finnish (Ilkka Tuohela), 
  French (Benoît Dejean, Damien Durand), 
  Hungarian (Gabor Kelemen), Slovak (Marcel Telka), 
  Nepali (Pawan Chitrakar)

2.3.15:

* Added optional starting_folder_uri parameter, to use, 
  for instance, when opening an examples folder.
  convenience method.
* g++ 4.01 build fixes.
* Translations: Added Ukranian (Maxim Dziumanenko),
  Greek (Kostas Papadimas), Chinese (Taiwan) (Chao-Hsiung Liao),
  Chinese (China) (Funda Wang), British English (Christopher Orr), 
  Spanish (Francisco Javier F. Serrador), 
  Brazilian Portugese (Raphael Higino), 
  Canadian English (Adam Weinberger), Kinyarwanda (Steve Murphy),
  Norwegian (Marcel Telka)
 
2.3.14:

* Document_XML::set_node_attribute_value(): Create more 
  compact documents, by not writing empty attributes. 
  That ignores default values, but this is only a 

2.3.13:

* Added App_Gtk::util_bold_message() to help avoid marking
  Pango markup for translation.
* Translations: Swedish started. (Christian Rose)

2.3.12:

* Now requires gtkmm 2.6.
* Dialogs are now more HIG-compliant, using secondary text,
  and are transient for the application window.
* BusyCursors: Might now work when they are nested.
  (Murray Cumming)

2.3.11:

* App_WithDoc::on_document_load() now returns a bool so that 
  the application (as well as the document class) also has 
  a chance to say whether the loaded document is OK. 
  NOTE: You need to change this in your applications - 
  otherwise your method overrides will no longer be overrides.

2.3.10:

* Fix a g++ 3.4 warning. (Murray Cumming)

2.3.9:

* Identify existing document even when the user does not specify 
  the extension in a new filename. 
* Prevent crash when there is no toolbar.
* Documentation: Mention the need to register the MIME type.
  (Murray Cumming)

2.3.8:

* Views inherit from sigc::trackable, to avoid crashes when calling dead 
  signal handlers.
  (Murray Cumming)

2.3.7:

* Document:
  - Create new documents with sensible (rather than bizarre) permissions.
  - Add parameter to the modified signal, and send it whenever it changes.
  (Murray Cumming)
  
2.3.6:

* Use URIs instead of filepaths, and use gnome-vfs instead of 
  C++ file IO. This means that bakery-based applications can 
  now open and save documents on networked servers. 
* Document: Added get_read_only().
  (Murray Cumming)

2.3.5:

* Document_XML: Added set_write_formatted().

2.3.4:

* gcc 3.4 build fixes. (Bryan Forbes)
* Added contructors for use with libglademm's get_widget_derived().
* App_WithDoc: Added add_mime_type(), which should be used to filter
  the recent-files list, but there seem to be some gnome-vfs bugs
  that stop it from working.
* Document: Added signal_forget(), so the view can null its Document 
  pointers when it is deleted. Surely there's a better way.
  (Murray Cumming)


2.3.3:

* EggRecentFiles header hidden from our public headers.
  (Bryan Forbes)
* Cleanup.
  (Bryan Forbes)
* Added "Others" placeholder, so that derived apps can
  insert their menus to the left of the Help menu.
  (Murray Cumming)
* Use a bakery prefix on the action names.
  (Murray Cumming)

2.3.2:

* Now uses UIManager API and the new FileChooser. (Bryan Forbes)
* Now uses libegg recent-files API, and therefore depends on gnome-vfsmm.
  (Murray Cumming, Bryan Forbes)
* App: added an overloaded set_about_information to set
  documenters and translator credits.
  (Bryan Forbes)

2.3.1:

* Updated for latest gtkmm 2.4 API.

2.3.0

* Now uses gtkmm 2.4 (and libsigc++ 2)
* This is Bakery 2.3.x, which will become 2.4.x.
  It can be installed in parallel with Bakery 2.0.
* Added Bakery::shared_ptr<> generic reference-counting shared smartpointer.
  (Murray Cumming)

2.0.0:

* bakery_gnomeui is now API/ABI stable.

1.3.11:

* RPM spec file added. (Eric Bourque)
* gconf data is now stored in the "apps" gconf sub-directory. That's what other GNOME apps do.
  (Bryan Forbes)
* Updated for latest libxml++ API. (Bryan Forbes)

1.3.10:

* Updated for latest libxml++ API. (Murray Cumming)

1.3.9:

* "Save Changes?" dialog:
  - Don't ignore the user's choice.
  - Don't remember a previous Cancel.
  (Matthew Tuck)
* Fixed segfault when saving unsaved changes when closing.
  (Murray Cumming)
* Fixed gettext includes problem in examples on some platforms.
  (Matthew Tuck)

1.3.8:

* Bakery::App_Gtk, App_WithDoc_Gtk: Moved some code into a separate GtkDialogs class,
  so that Bakery_GnomeUI can reuse it. (Murray Cumming)
* Corrected button order so that [Save] is on the right. (Murray Cumming)
* Bakery::Conf::Client: Fixed Association lifetime. (Michael Johnson)

1.3.7:

* Bakery no longer depends on libgnomeuimm.
* Bakery::App and Bakery::App_WithDoc are now independent of any GUI toolkit.
  The previous functionality is available via Bakery::App_GnomeUI or 
  Bakery::App_WithDoc_GnomeUI in the bakery_gnomeui package, or you can use
  Bakery::App_Gtk and Bakery::App_WithDoc_Gtk instead. 
  
  So to update your code for the new API:
    - Replace App_WithDoc with App_WithDoc_Gtk throughout your own code.
    - Use Bakery::App_Gtk::add() instead of Gnome::UI::App::set_contents()
    - Use the gtkmm menus and toolbars APIs instead of the GNOME ones.
    - Use Gtk::Main instead of Gnome::Main and call Bakery::init().
    - OR use the bakery_gnomeui extension library.

* Bakery::ConfClient renamed to Bakery::Conf::Client. This and 
  Bakery::Dialog_Preferences can now handle many more types of widgets.
  (Michael Johnson)

* intl-* files distributed.#
* Updated for latest libxml++ API.

1.3.6:

* All methods are now lowercase. Be careful that your on_document_load()
  override is still an overrides.
* Document_XML: Fix delayed save bug by overriding save_before() instead
  of save(). That's what's supposd to be overriden anyway.
* Replaced all use of gnome_config_* with gconfmm. For instance, the 
  "Recent Documents".
* Mysterious gnome-i18n.h warnings fixes.

1.3.5:

* gcc 3.2 fix. (Matthew Tuck)
* Fixed warning in WithXmlDoc example (Murray Cumming)

1.3.4:

* Added Bakery::Document_XML, and a WithXmlDoc example.
  This allows Bakery applications to use XML for their document's structure, 
  using libxml++. This is based on the Document_XML class that was in the 
  bakery_example_xml package, but the Xerces-C++ parser has been replaced with 
  libxml++ because Xerces-C++ are not serious about API stability or packaging.
  (Murray Cumming) 
* The recent files menu item works again. (Murray Cumming)
* The About box can be shown more than once. (Matthew Tuck)
* gcc 3.2 warnings fixes. (Matthew Tuck)
