/*
 * Copyright 2002 Murray Cumming
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef BAKERY_INIT_H
#define BAKERY_INIT_H

namespace Bakery
{

/** Saves you the trouble of initializing gconfmm and libglademm yourself.
 * You still need to have a Gtk::Main instance though.
 * You don't need this if you used Gnome::Main instead of Gtk::Main, because that
 * initializes gconfmm and libglademm also.
 */
void init();

} //namespace

#endif //BAKERY_INIT_H
