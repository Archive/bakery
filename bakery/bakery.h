/*
 * Copyright 2000 Murray Cumming
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef BAKERY_H
#define BAKERY_H

extern const unsigned int bakery_major_version;
extern const unsigned int bakery_minor_version;
extern const unsigned int bakery_micro_version;

#include <bakeryconfig.h>
#include <bakery/init.h>
#include <bakery/App/App.h>
#include <bakery/App/App_WithDoc.h>
#include <bakery/App/App_Gtk.h>
#include <bakery/App/App_WithDoc_Gtk.h>
#include <bakery/Document/Document_XML.h>
#include <bakery/View/View.h>
#include <bakery/View/View_Composite.h>
#include <bakery/Configuration/Dialog_Preferences.h>
#include <bakery/Utilities/BusyCursor.h>
#include <gtkmm.h>

#endif //BAKERY_H
