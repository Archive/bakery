/* examplewindow.cc
 *
 * Copyright (C) 2002 Murray Cumming
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "examplewindow.h"

ExampleWindow::ExampleWindow()
: m_Button("Start Processing.")
{
  add(m_Button);

  //Connect signals:
  m_Button.signal_clicked().connect( sigc::mem_fun(*this, &ExampleWindow::on_button_clicked) );

  show_all_children();
}


ExampleWindow::~ExampleWindow()
{

}

void ExampleWindow::on_button_clicked()
{
  Bakery::BusyCursor cursor(*this);

  for(long i = 0; i < 10000; i++)
  {
    //Test an inner BusyCursor:
    {
      Bakery::BusyCursor cursor(*this);
    }

    //Using std::cout should take some time:
    std::cout << "processing" << std::endl;
 
    for(long l = 0; l < 10; l++)
    {
      //Using std::cout should take some time:
      std::cout << "inner processing" << i << ", " << l << std::endl;
 
    }
    
  }
}


