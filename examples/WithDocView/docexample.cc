// -*- C++ -*-

/* docexample.cc
 * 
 * Copyright (C) 2000 Murray Cumming  
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "docexample.h"

DocExample::DocExample()
{
  set_file_extension("bakery_withdocview");
}

DocExample::~DocExample()
{
}

bool DocExample::load_after()
{
  bool bTest = Bakery::Document::load_after();

  if(bTest)
  {
	  //See comment in save_before().
	  m_strSomething = get_contents();
  } 

  return bTest; 
}

bool DocExample::save_before()
{
  //You would ususally save your many pieces of data in some kind of file format,
  //e.g. XML.
  set_contents(m_strSomething);
  return Bakery::Document::save_before();
}

void DocExample::set_something(const Glib::ustring& strSomething)
{
  m_strSomething = strSomething;

  set_modified();
}

Glib::ustring DocExample::get_something() const
{
  return m_strSomething;
}
