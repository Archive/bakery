// -*- C++ -*-

/* appexample.h
 * 
 * Copyright (C) 2000 Murray Cumming  
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef HEADER_APP_EXAMPLE
#define HEADER_APP_EXAMPLE

#include "bakery/bakery.h"
#include "docexample.h"
#include "viewexample.h"

//Inherit from Bakery::App_WithDoc for Document support.
class AppExample : public Bakery::App_WithDoc_Gtk
{
public:
  AppExample();
  virtual ~AppExample();

  virtual void init();  //override.

protected:
  virtual void init_create_document(); //override
  virtual App* new_instance(); //override

  //Child widgets:
  ViewExample m_View;
};

#endif //HEADER_APP_EXAMPLE
