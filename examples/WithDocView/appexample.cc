// -*- C++ -*-

/* appexample.cc
 * 
 * Copyright (C) 2000 Murray Cumming 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "config.h"
#include "appexample.h"
#include <glibmm/i18n.h>

AppExample::AppExample()
  : Bakery::App_WithDoc_Gtk("WithDocView")
{
}

AppExample::~AppExample()
{
}

void AppExample::init()
{
  type_vecStrings vecAuthors;
  vecAuthors.push_back("Murray Cumming <murrayc@usa.net>");
  set_about_information("0.1", vecAuthors, "(C) 2000 Murray Cumming", _("A Bakery example."));

  //Call base method:
  Bakery::App_WithDoc_Gtk::init();

  add(m_View);
}

Bakery::App* AppExample::new_instance()
{
  AppExample* pApp = new AppExample();
  return pApp;
}

void AppExample::init_create_document()
{
  if(!m_pDocument)
  {
    m_pDocument = new DocExample();

    //Tell document about view:
    m_pDocument->set_view(&m_View);

    //Tell view about document:
    m_View.set_document(static_cast<DocExample*>(m_pDocument));
  }
  
  Bakery::App_WithDoc_Gtk::init_create_document(); //Sets window title. Doesn't recreate doc.
}
