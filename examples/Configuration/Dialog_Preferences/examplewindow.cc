//$Id$ -*- c++ -*-

/* gtkmm example Copyright (C) 2002 gtkmm development team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "config.h"
#include "examplewindow.h"
#include "exampledialog.h"
#include <iostream>
#include <glibmm/i18n.h>


ExampleWindow::ExampleWindow()
: m_Button_Preferences_Delayed(_("Preferences (apply on close)")),
  m_Button_Preferences_Instant(_("Preferences (instant apply)"))
{  
  // Sets the border width of the window.
  set_border_width(10);

  add(m_VBox);
  m_VBox.pack_start(m_Button_Preferences_Delayed);
  m_VBox.pack_start(m_Button_Preferences_Instant);

  //Connect button's signal handler:
  m_Button_Preferences_Delayed.signal_clicked().connect( sigc::mem_fun(*this, &ExampleWindow::on_delayed_button_clicked) );
  m_Button_Preferences_Instant.signal_clicked().connect( sigc::mem_fun(*this, &ExampleWindow::on_instant_button_clicked) );

  show_all_children();
}

ExampleWindow::~ExampleWindow()
{
}

void ExampleWindow::on_delayed_button_clicked()
{
  //Show the preferences dialog:
  ExampleDialog preferences(*this, false);
  preferences.run();
}

void ExampleWindow::on_instant_button_clicked()
{
  //Show the preferences dialog:
  ExampleDialog preferences(*this,true);
  preferences.run();
}
