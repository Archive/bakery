/* examplewindow.cc
 *
 * Copyright (C) 2002 Murray Cumming
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include "config.h"
#include "examplewindow.h"
#include <glibmm/i18n.h>

ExampleWindow::ExampleWindow()
: m_Frame_Instant(_("Instant-apply")), m_Frame_NotInstant(_("Load/Save")),
  m_Table_Instant(1, 2), m_Table_NotInstant(2, 3),
  m_Label_Instant(_("foo")), m_Label_NotInstant(_("bar")),
  m_Button_Load("Load"), m_Button_Save(Gtk::Stock::SAVE),
  m_ConfClient("/apps/gnome/bakery_examples/confclient")
{
  //Arrange the widgets:
  //This might be easier with libglademm, but that's not what I'm demonstrating here.
  add(m_VBox);

  m_VBox.pack_start(m_Frame_Instant);
  m_Frame_Instant.add(m_Table_Instant);
  m_Table_Instant.attach(m_Label_Instant, 0, 1, 0, 1);
  m_Table_Instant.attach(m_Entry_Instant, 1, 2, 0, 1);

  m_VBox.pack_start(m_Frame_NotInstant);
  m_Frame_NotInstant.add(m_Table_NotInstant);
  m_Table_NotInstant.attach(m_Label_NotInstant, 0, 1, 0, 1);
  m_Table_NotInstant.attach(m_Entry_NotInstant, 1, 2, 0, 1);
  m_Table_NotInstant.attach(m_Button_Load, 0, 2, 1, 2);
  m_Table_NotInstant.attach(m_Button_Save, 0, 2, 2, 3);

  //GConf:
  m_ConfClient.add_instant("foo", m_Entry_Instant);
  m_ConfClient.add("bar", m_Entry_NotInstant);

  //Connect signals:
  m_Button_Load.signal_clicked().connect( sigc::mem_fun(*this, &ExampleWindow::on_button_load) );
  m_Button_Save.signal_clicked().connect( sigc::mem_fun(*this, &ExampleWindow::on_button_save) );

  show_all_children();
}


ExampleWindow::~ExampleWindow()
{

}

void ExampleWindow::on_button_load()
{
#ifdef GLIBMM_EXCEPTIONS_ENABLED
  m_ConfClient.load();
#else
  std::auto_ptr<Glib::Error> error;
  m_ConfClient.load(error);
#endif
}

void ExampleWindow::on_button_save()
{
#ifdef GLIBMM_EXCEPTIONS_ENABLED
  m_ConfClient.save();
#else
  std::auto_ptr<Glib::Error> error;
  m_ConfClient.save(error);
#endif
}
