/* examplewindow.hh
 *
 * Copyright (C) 2002 Murray Cumming
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef HEADER_APP_EXAMPLE
#define HEADER_APP_EXAMPLE

#include <bakery/bakery.h>

class ExampleWindow : public Gtk::Window
{
public:
  ExampleWindow();
  virtual ~ExampleWindow();

protected:

  //Signal handlers:
  virtual void on_button_load();
  virtual void on_button_save();

  //Member widgets
  Gtk::VBox m_VBox;
  Gtk::Frame m_Frame_Instant, m_Frame_NotInstant;
  Gtk::Table m_Table_Instant, m_Table_NotInstant;
  Gtk::Entry m_Entry_Instant, m_Entry_NotInstant;
  Gtk::Label m_Label_Instant, m_Label_NotInstant;

  Gtk::Button m_Button_Load, m_Button_Save;

  Bakery::Conf::Client m_ConfClient;
};

#endif //HEADER_APP_EXAMPLE

