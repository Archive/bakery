// -*- C++ -*-

/* appexample.cc
 * 
 * Copyright (C) 2000 Murray Cumming 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "config.h"
#include "appexample.h"
#include <glibmm/i18n.h>

AppExample::AppExample()
  : Bakery::App_WithDoc_Gtk("WithDoc"),
  m_Label("blah")
{
  m_Box.pack_start(m_Label, Gtk::PACK_EXPAND_WIDGET);
  m_Box.pack_start(m_Entry, Gtk::PACK_EXPAND_WIDGET);

  //Connect signals:
  m_Entry.signal_changed().connect(sigc::mem_fun(*this, &AppExample::on_Entry_changed));
 
  m_Box.show_all();
}

AppExample::~AppExample()
{
}

void AppExample::init()
{
  type_vecStrings vecAuthors;
  vecAuthors.push_back("Murray Cumming <murrayc@usa.net>");
  set_about_information("0.1", vecAuthors, "(C) 2000 Murray Cumming", _("A Bakery example."));

  //Call base method:
  Bakery::App_WithDoc_Gtk::init();

  add(m_Box);
}

Bakery::App* AppExample::new_instance()
{
  AppExample* pApp = new AppExample();
  return pApp;
}

void AppExample::init_create_document()
{
  if(m_pDocument == NULL)
  {
    m_pDocument = new DocExample();
  }
  
  Bakery::App_WithDoc_Gtk::init_create_document(); //Sets window title. Doesn't recreate doc.
}

void AppExample::on_Entry_changed()
{
  //Set the data in the document, from the User Interface:
  //This would be simpler if we used a View - see the other App_WithDocView example.
  DocExample* pDocument = static_cast<DocExample*>(get_document());
  pDocument->set_something(m_Entry.get_text());
}

bool AppExample::on_document_load()
{      
  //Show the data from the document in the GUI:
  //This would be simpler if we used a View - see the other App_WithDocView example.
  DocExample* pDocument = static_cast<DocExample*>(get_document());
  m_Entry.set_text(pDocument->get_something());

  return Bakery::App_WithDoc::on_document_load();
}
