// -*- C++ -*-

/* subview1.h
 * 
 * Copyright (C) 2000 Murray Cumming  
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "subview1.h"

SubView1::SubView1()
:  m_Label("blah")
{
  pack_start(m_Label);
  pack_start(m_Entry);

  //Connect signals:
  m_Entry.signal_changed().connect(sigc::mem_fun(*this, &SubView1::on_Entry_changed));
 
  show_all();
}

SubView1::~SubView1()
{
}

void SubView1::load_from_document()
{
  m_Entry.set_text( get_document()->get_something() );
}

void SubView1::save_to_document()
{
  const Glib::ustring& strText =  m_Entry.get_text();

  if(strText != get_document()->get_something()) //Don't trigger 'modified' unless it really is different.
    get_document()->set_something(strText);
}

void SubView1::on_Entry_changed()
{
  //The "changed" signal is actually sent for every key press in the Entry, so you would not actually do this in a real application.
  //But to get notification that the editing has finished you need to derive a new Entry class, and that would complicate this example.
  
  //We don't really need to do this, but it updates the modified status,
  //and a more complicated View might need its Document updated all the time.
  save_to_document();
}
