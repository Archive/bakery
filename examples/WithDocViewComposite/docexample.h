// -*- C++ -*-

/* docexample.h
 * 
 * Copyright (C) 2000 Murray Cumming  
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef HEADER_DOC_EXAMPLE
#define HEADER_DOC_EXAMPLE

#include "bakery/bakery.h"

class DocExample : public Bakery::Document
{
public:
  DocExample();
  virtual ~DocExample();

  //overrides:
  virtual bool load_after();
  virtual bool save_before();

  void set_something(const Glib::ustring& strSomething);
  Glib::ustring get_something() const;

  void set_something_else(const Glib::ustring& strSomethingElse);
  Glib::ustring get_something_else() const;

protected:
  Glib::ustring m_strSomething, m_strSomethingElse;

};


#endif //HEADER_DOC_EXAMPLE
