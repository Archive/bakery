// -*- C++ -*-

/* subview2.h
 * 
 * Copyright (C) 2000 Murray Cumming  
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "subview2.h"

SubView2::SubView2()
:  m_Label("foo")
{
  //TODO: This bottom pane is horribly high:
  attach(m_Label, 0, 1, 0, 1);
  attach(m_Combo, 1, 2, 0, 1);

  //Set up combo:
  m_Combo.append_text("abc");
  m_Combo.append_text("def");
  m_Combo.append_text("ghi");
  
  //Connect signals:
  m_Combo.signal_changed().connect(sigc::mem_fun(*this, &SubView2::on_Combo_changed));
 
  show_all();
}

SubView2::~SubView2()
{
}

void SubView2::load_from_document()
{
  m_Combo.set_active_text( get_document()->get_something_else() );
}

void SubView2::save_to_document()
{
  const Glib::ustring strText = m_Combo.get_active_text();
  get_document()->set_something_else(strText);
}

void SubView2::on_Combo_changed()
{
  //We don't really need to do this, but it updates the modified status,
  //and a more complicated View might need its Document updated all the time.
  save_to_document();
}
