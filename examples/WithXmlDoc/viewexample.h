// -*- C++ -*-

/* viewexample.h
 * 
 * Copyright (C) 2000 Murray Cumming  
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef HEADER_VIEW_EXAMPLE
#define HEADER_VIEW_EXAMPLE

#include <gtkmm.h>
#include "docexample.h"

class ViewExample : 
  public Gtk::VBox,
  public Bakery::View<DocExample>
{
public:
  ViewExample();
  virtual ~ViewExample();

  //overrrides:
  virtual void load_from_document();
  virtual void save_to_document();

protected:

  //Signal handlers:
  virtual void on_Entry_changed();

  //Child widgets:
  Gtk::Label m_Label;
  Gtk::Entry m_Entry;
};

#endif //HEADER_VIEW_EXAMPLE
