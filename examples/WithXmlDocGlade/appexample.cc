// -*- C++ -*-

/* appexample.cc
 * 
 * Copyright (C) 2000 Murray Cumming 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <bakeryconfig.h>
#include "config.h"
#include "appexample.h"
#include <glibmm/i18n.h>

AppExample::AppExample(BaseObjectType* cobject, const Glib::RefPtr<Gnome::Glade::Xml>& refGlade)
: Bakery::App_WithDoc_Gtk(cobject, "Bakery_Example_Glade"),
  ParentWindow(cobject), //This is a virtual base class (not a direct base), so we must specify a constructor or the default constructor will be called, regardless of what the App_Gtk(cobject) constructor does. Derived classes must do this as well.
  m_pView(0)
{
  //Get the glade-instantiated vbox.
  //Your .glade file _must_ contain a GtkVBox called "bakery_vbox".
  //init_layout() will then insert the menus and toolbars into the vbox.
  refGlade->get_widget("bakery_vbox", m_pVBox);

  //Get the glade-instantiated view:
  refGlade->get_widget_derived("vbox_border", m_pView);
}

AppExample::~AppExample()
{
}

void AppExample::init()
{
  type_vecStrings vecAuthors;
  vecAuthors.push_back("Murray Cumming <murrayc@usa.net>");
  set_about_information("1.0.0", vecAuthors, "(C) 2000 Murray Cumming", _("A Bakery XML example, also using libglademm."));

  //Call base method:
  Bakery::App_WithDoc_Gtk::init();
}

Bakery::App* AppExample::new_instance()
{
#ifdef GLIBMM_EXCEPTIONS_ENABLED
  Glib::RefPtr<Gnome::Glade::Xml> refXml = Gnome::Glade::Xml::create("example.glade", "window_main");
#else
  std::auto_ptr<Gnome::Glade::XmlError> error;
  Glib::RefPtr<Gnome::Glade::Xml> refXml = Gnome::Glade::Xml::create("example.glade", "window_main", "", error);
  if(error.get()) return 0;
#endif
  AppExample* pApp = 0;
  refXml->get_widget_derived("window_main", pApp);
  
  return pApp;
}

void AppExample::init_create_document()
{
  if(!m_pDocument)
  {
    m_pDocument = new DocExample();

    //Tell document about view:
    m_pDocument->set_view(m_pView);

    //Tell view about document:
    m_pView->set_document(static_cast<DocExample*>(m_pDocument));
  }
  
  Bakery::App_WithDoc_Gtk::init_create_document(); //Sets window title. Doesn't recreate doc.
}
