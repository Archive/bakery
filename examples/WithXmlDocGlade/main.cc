// -*- C++ -*-

/* main.cc
 *
 * Copyright (C) 2000 Murray Cumming
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "appexample.h"

int
main(int argc, char* argv[])
{
  Gtk::Main mainWithoutDoc(argc, argv);

  Bakery::init();

  //Instantiate the window from the libglade file.
  //In a real application, the .glade file would be installed, and the path would be in a #define.
#ifdef GLIBMM_EXCEPTIONS_ENABLED
  Glib::RefPtr<Gnome::Glade::Xml> refXml = Gnome::Glade::Xml::create("example.glade", "window_main");
#else
  std::auto_ptr<Gnome::Glade::XmlError> error;
  Glib::RefPtr<Gnome::Glade::Xml> refXml = Gnome::Glade::Xml::create("example.glade", "window_main", "", error);
  if(error.get())
  {
    std::cerr << error->what() << std::endl;
    return -1;
  }
#endif
  AppExample* pApp = 0;
  refXml->get_widget_derived("window_main", pApp);
    
  AppExample::set_command_line_args(argc, argv);
  pApp->init(); //Sets it up and shows it.

  mainWithoutDoc.run();

  return 0;
}
